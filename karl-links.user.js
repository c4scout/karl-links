// ==UserScript==
// @name        Karl Links
// @namespace   DRG
// @description Linkifies DRG Overclock modcodes
// @include     https://boards.4channel.org/v/thread/*
// @include     https://boards.4channel.org/vm/thread/*
// @include     https://boards.4channel.org/vg/thread/*
// @include     https://arch.b4k.co/v/thread/*
// @include     https://arch.b4k.co/v/search/text/*
// @include     https://arch.b4k.co/vm/thread/*
// @include     https://arch.b4k.co/vm/search/text/*
// @include     https://arch.b4k.co/vg/thread/*
// @include     https://arch.b4k.co/vg/search/text/*
// @include     https://archived.moe/v/thread/*
// @include     https://archived.moe/vg/thread/*
// @version     1.0.0
// @run-at      document-start
// ==/UserScript==
 
(function () {
  'use strict';

  const CHECK_OP = true; // If Enabled, try to only run on DRG threads
  const OP_REGEX = new RegExp("(DRG|Deep Rock|Deep Rock Galactic|Rock and Stone|Karl|Leaf Lover|Glyphid)", "gi");

  const DRG_REGEX = new RegExp("([0-9xX]{5}) (?:w/ |with |and )?(([a-zA-Z]+)( [a-zA-Z]+){0,2})", "gi");
  const DRG_CLASS = 'karl-build';
  const DRG_INVALID_CLASS = 'karl-invalid-build';

  const HOSTNAME = document.location.hostname;

  let ROOT_CLASS;
  let TEXT_CLASS;
  let GREENTEXT_CLASS;
  let SUBJECT_CLASS;
  let OP_POST_CLASS;
  let DRG_NYI_CLASS;

  switch (HOSTNAME) {
    case "boards.4channel.org":
      ROOT_CLASS = "thread";
      TEXT_CLASS = "postMessage";
      GREENTEXT_CLASS = "quote";
      SUBJECT_CLASS = "subject";
      OP_POST_CLASS = "op";
      DRG_NYI_CLASS = "warning";
      break;
    case "arch.b4k.co":
    case "archived.moe":
      ROOT_CLASS = "posts";
      TEXT_CLASS = "text";
      GREENTEXT_CLASS = "greentext";
      SUBJECT_CLASS = "post_title";
      OP_POST_CLASS = "post_is_op";
      DRG_NYI_CLASS = "banned";
      break;
    default:
      console.log("Invalid Hostname");
      break;
  }
  
  const css = `
.karl-build::before {
  content: "";
  background: transparent url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAMCAYAAACulacQAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAAYRpQ0NQSUNDIHByb2ZpbGUAACiRfZE9SMNAHMVfW7UiFUE7iDhkqC5akCriqFUoQoVQK7TqYHLpFzRpSFJcHAXXgoMfi1UHF2ddHVwFQfADxNHJSdFFSvxfUmgR48FxP97de9y9A/z1MlPNjglA1SwjlYgLmeyqEHxFF0LoxxhiEjP1OVFMwnN83cPH17soz/I+9+foVXImA3wC8SzTDYt4g3h609I57xOHWVFSiM+Jxw26IPEj12WX3zgXHPbzzLCRTs0Th4mFQhvLbcyKhko8RRxRVI3y/RmXFc5bnNVylTXvyV8Yymkry1ynOYwEFrEEEQJkVFFCGRaitGqkmEjRftzDP+T4RXLJ5CqBkWMBFaiQHD/4H/zu1sxPxtykUBzofLHtjxEguAs0arb9fWzbjRMg8AxcaS1/pQ7MfJJea2mRI6BvG7i4bmnyHnC5Aww+6ZIhOVKApj+fB97P6JuywMAt0LPm9tbcx+kDkKaukjfAwSEwWqDsdY93d7f39u+ZZn8/sUBywNbO9j8AAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfmBgwSMg+rMibZAAAABmJLR0QA/wD/AP+gvaeTAAAAt0lEQVR4AQXBrzJEURwA4O/ePWPMDoUgCt7AbN0kCZpRCKIgeArvYAUvoUqCpKiC8WdGMVvsnnvPsXd+vq+JA2eYgnCtONXb1XsUT27iXcSXiGNH8SDiVcSlk9aoYRs72HNuH9WPF/etu7jwC5g6NMKzsQ1XDcSnsIU/DJjjzSyBig4dYMAmLShY4dbE3ExGIYGMNRQUZHQkkJGwQocFKgn0SCjosMRAAhlQkbEEEvjGOio+sEDLP989SY8zP2jDAAAAAElFTkSuQmCC') center left no-repeat!important;
  padding-left: 12px;
}
`

  const OVERCLOCKS = {
    /* -------- */
    /* ENGINEER */
    /* -------- */
    /* WARTHOG */
    "stunner": { oc_id: 1, w_id: 1 },
    "lightweight magazines": { oc_id: 2, w_id: 1 },
    "light-weight magazines": { oc_id: 2, w_id: 1 },
    "light weight magazines": { oc_id: 2, w_id: 1 },
    "lwm": { oc_id: 2, w_id: 1 },
    "magnetic pellet alignment": { oc_id: 3, w_id: 1 },
    "magnetic pellet": { oc_id: 3, w_id: 1 },
    "mpa": { oc_id: 3, w_id: 1 },
    "cycle overload": { oc_id: 4, w_id: 1 },
    "cycle": { oc_id: 4, w_id: 1 },
    "co": { oc_id: 4, w_id: 1 },
    "mini shells": { oc_id: 5, w_id: 1 },
    "minishells": { oc_id: 5, w_id: 1 },
    "ms": { oc_id: 5, w_id: 1 },
    /* STUBBY */
    "super-slim rounds": { oc_id: 1, w_id: 2 },
    "super slim rounds": { oc_id: 1, w_id: 2 },
    "superslim rounds": { oc_id: 1, w_id: 2 },
    "ssr": { oc_id: 1, w_id: 2 },
    "well oiled machine": { oc_id: 2, w_id: 2 },
    "wom": { oc_id: 2, w_id: 2 },
    "em refire booster": { oc_id: 3, w_id: 2 },
    "em refire": { oc_id: 3, w_id: 2 },
    "emrb": { oc_id: 3, w_id: 2 },
    "emr": { oc_id: 3, w_id: 2 },
    "light-weight rounds": { oc_id: 4, w_id: 2 },
    "light weight rounds": { oc_id: 4, w_id: 2 },
    "lightweight rounds": { oc_id: 4, w_id: 2 },
    "lwr": { oc_id: 4, w_id: 2 },
    "turret arc": { oc_id: 5, w_id: 2 },
    "turret em discharge": { oc_id: 6, w_id: 2 },
    "em discharge": { oc_id: 6, w_id: 2 },
    "turret em": { oc_id: 6, w_id: 2 },
    "ted": { oc_id: 6, w_id: 2 },
    /* PGL */
    "clean sweep": { oc_id: 1, w_id: 3 },
    "cs": { oc_id: 1, w_id: 3 },
    "pack rat": { oc_id: 2, w_id: 3 },
    "pr": { oc_id: 2, w_id: 3 },
    "compact rounds": { oc_id: 3, w_id: 3 },
    "cr": { oc_id: 3, w_id: 3 },
    "rj253 compound": { oc_id: 4, w_id: 3 },
    "rj compound": { oc_id: 4, w_id: 3 },
    "rj253": { oc_id: 4, w_id: 3 },
    "rocket jump": { oc_id: 4, w_id: 3 },
    "rj": { oc_id: 4, w_id: 3 },
    "fat boy": { oc_id: 5, w_id: 3 },
    "fatboy": { oc_id: 5, w_id: 3 },
    "nuke": { oc_id: 5, w_id: 3 },
    "fb": { oc_id: 5, w_id: 3 },
    "hyper propellant": { oc_id: 6, w_id: 3 },
    "hyper prop": { oc_id: 6, w_id: 3 },
    "hyperprop": { oc_id: 6, w_id: 3 },
    "hp": { oc_id: 6, w_id: 3 },
    /* BREACH CUTTER */
    "light-weight cases": { oc_id: 1, w_id: 4 },
    "light weight cases": { oc_id: 1, w_id: 4 },
    "lightweight cases": { oc_id: 1, w_id: 4 },
    "lwc": { oc_id: 1, w_id: 4 },
    "roll control": { oc_id: 2, w_id: 4 },
    "rc": { oc_id: 2, w_id: 4 },
    "stronger plasma current": { oc_id: 3, w_id: 4 },
    "stronger plasma": { oc_id: 3, w_id: 4 },
    "spc": { oc_id: 3, w_id: 4 },
    "return to sender": { oc_id: 4, w_id: 4 },
    "rts": { oc_id: 4, w_id: 4 },
    "high voltage crossover": { oc_id: 5, w_id: 4 },
    "high voltage": { oc_id: 5, w_id: 4 },
    "hvc": { oc_id: 5, w_id: 4 },
    "spinning death": { oc_id: 6, w_id: 4 },
    "spin": { oc_id: 6, w_id: 4 },
    "sd": { oc_id: 6, w_id: 4 },
    "inferno": { oc_id: 7, w_id: 4 },
    /* LOK1 */
    "armor break module": { oc_id: 1, w_id: 17 },
    "abm": { oc_id: 1, w_id: 17 },
    "eraser": { oc_id: 2, w_id: 17 },
    "seeker rounds": { oc_id: 3, w_id: 17 },
    "seeker": { oc_id: 3, w_id: 17 },
    "sr": { oc_id: 3, w_id: 17 },
    "explosive chemical rounds": { oc_id: 4, w_id: 17 },
    "explosive chemical": { oc_id: 4, w_id: 17 },
    "ecr": { oc_id: 4, w_id: 17 },
    "executioner": { oc_id: 5, w_id: 17 },
    "exec": { oc_id: 5, w_id: 17 },
    "exe": { oc_id: 5, w_id: 17 },
    "neuro-lasso": { oc_id: 6, w_id: 17 },
    "neuro lasso": { oc_id: 6, w_id: 17 },
    "lasso": { oc_id: 6, w_id: 17 },
    /* SHARD DIFFRACTOR */  
    "efficiency tweaks": { oc_id: 1, w_id: 22, nyi: true },
    "et": { oc_id: 1, w_id: 22, nyi: true },
    "automated beam controller": { oc_id: 2, w_id: 22, nyi: true },
    "auto beam controller": { oc_id: 2, w_id: 22, nyi: true },
    "auto beam": { oc_id: 2, w_id: 22, nyi: true },
    "abc": { oc_id: 2, w_id: 22, nyi: true },
    "feedback loop": { oc_id: 3, w_id: 22, nyi: true },
    "fl": { oc_id: 3, w_id: 22, nyi: true },
    "volatile impact reactor": { oc_id: 4, w_id: 22, nyi: true },
    "vir": { oc_id: 4, w_id: 22, nyi: true },
    "endothermic explosion": { oc_id: 5, w_id: 22, nyi: true },
    "endothermic": { oc_id: 5, w_id: 22, nyi: true },
    "plastcrete catalyst": { oc_id: 5, w_id: 22, nyi: true },
    "plastcrete": { oc_id: 5, w_id: 22, nyi: true },
    "ee": { oc_id: 5, w_id: 22, nyi: true },
    "overdrive booster": { oc_id: 6, w_id: 22, nyi: true },
    "overdrive": { oc_id: 6, w_id: 22, nyi: true },
    "ob": { oc_id: 6, w_id: 22, nyi: true },
    /* ------- */
    /* DRILLER */
    /* ------- */
    /* CRSPR */
    "lighter tanks": { oc_id: 1, w_id: 9 },
    "sticky additive": { oc_id: 2, w_id: 9 },
    "sa": { oc_id: 2, w_id: 9 },
    "compact feed valves": { oc_id: 3, w_id: 9 },
    "cfv": { oc_id: 3, w_id: 9 },
    "fuel stream diffuser": { oc_id: 4, w_id: 9 },
    "fuel stream": { oc_id: 4, w_id: 9 },
    "fsd": { oc_id: 4, w_id: 9 },
    "face melter": { oc_id: 5, w_id: 9 },
    "facemelter": { oc_id: 5, w_id: 9 },
    "facemelt": { oc_id: 5, w_id: 9 },
    "sticky fuel": { oc_id: 6, w_id: 9 },
    "sticky flames": { oc_id: 6, w_id: 9 },
    "sf": { oc_id: 6, w_id: 9 },
    /* CRYO */
    "improved thermal efficiency": { oc_id: 1, w_id: 10 },
    "ite": { oc_id: 1, w_id: 10 },
    "tuned cooler": { oc_id: 2, w_id: 10 },
    "tc": { oc_id: 2, w_id: 10 },
    "flow rate expansion": { oc_id: 3, w_id: 10 },
    "fre": { oc_id: 3, w_id: 10 },
    "ice spear": { oc_id: 4, w_id: 10 },
    "ice storm": { oc_id: 5, w_id: 10 },
    "snowball": { oc_id: 6, w_id: 10 },
    /* SLUDGE */
    "hydrogen ion additive": { oc_id: 1, w_id: 19 },
    "hydrogen ion": { oc_id: 1, w_id: 19 },
    "hia": { oc_id: 1, w_id: 19 },
    "ag mixture": { oc_id: 2, w_id: 19 },
    "agm": { oc_id: 2, w_id: 19 },
    "volatile impact mixture": { oc_id: 3, w_id: 19 },
    "vim": { oc_id: 3, w_id: 19 },
    "disperser compound": { oc_id: 4, w_id: 19 },
    "disperser": { oc_id: 4, w_id: 19 },
    "dc": { oc_id: 4, w_id: 19 },
    "goo bomber special": { oc_id: 5, w_id: 19 },
    "goo bomber": { oc_id: 5, w_id: 19 },
    "gbs": { oc_id: 5, w_id: 19 },
    "sludge blast": { oc_id: 6, w_id: 19 },
    "sb": { oc_id: 6, w_id: 19 },
    /* SUBATA */
    "chain hit subata": { oc_id: 1, w_id: 11 },
    "homebrew powder subata": { oc_id: 2, w_id: 11 },
    "oversized magazine": { oc_id: 3, w_id: 11 },
    "om": { oc_id: 3, w_id: 11 },
    "automatic fire": { oc_id: 4, w_id: 11 },
    "auto fire": { oc_id: 4, w_id: 11 },
    "af": { oc_id: 4, w_id: 11 },
    "explosive reload": { oc_id: 5, w_id: 11 },
    "tranquilizer rounds": { oc_id: 6, w_id: 11 },
    "tranq rounds": { oc_id: 6, w_id: 11 },
    "tr": { oc_id: 6, w_id: 11 },
    /* EPC */
    "energy rerouting": { oc_id: 1, w_id: 12 },
    "magnetic cooling unit": { oc_id: 2, w_id: 12 },
    "mcu": { oc_id: 2, w_id: 12 },
    "heat pipe": { oc_id: 3, w_id: 12 },
    "hp": { oc_id: 3, w_id: 12 },
    "heavy hitter": { oc_id: 4, w_id: 12 },
    "hh": { oc_id: 4, w_id: 12 },
    "overcharger": { oc_id: 5, w_id: 12 },
    "persistent plasma": { oc_id: 6, w_id: 12 },
    /* CWC */
    "liquid cooling system": { oc_id: 1, w_id: 21, nyi: true },
    "lcs": { oc_id: 1, w_id: 21, nyi: true },
    "super focus lens": { oc_id: 2, w_id: 21, nyi: true },
    "sfl": { oc_id: 2, w_id: 21, nyi: true },
    "diffusion ray": { oc_id: 3, w_id: 21, nyi: true },
    "dr": { oc_id: 3, w_id: 21, nyi: true },
    "mega power supply": { oc_id: 4, w_id: 21, nyi: true },
    "mps": { oc_id: 4, w_id: 21, nyi: true },
    "blistering necrosis": { oc_id: 5, w_id: 21, nyi: true },
    "blistering": { oc_id: 5, w_id: 21, nyi: true },
    "blister": { oc_id: 5, w_id: 21, nyi: true },
    "necrosis": { oc_id: 5, w_id: 21, nyi: true },
    "bn": { oc_id: 5, w_id: 21, nyi: true },
    "gamma contamination": { oc_id: 6, w_id: 21, nyi: true },
    "gamma": { oc_id: 6, w_id: 21, nyi: true },
    "gc": { oc_id: 6, w_id: 21, nyi: true },
    /* ------ */
    /* GUNNER */
    /* ------ */
    /* LEADSTORM */
    "little more oomph": { oc_id: 1, w_id: 13 },
    "more oomph": { oc_id: 1, w_id: 13 },
    "lmo": { oc_id: 1, w_id: 13 },
    "thinned drum walls": { oc_id: 2, w_id: 13 },
    "thin drum walls": { oc_id: 2, w_id: 13 },
    "tdw": { oc_id: 2, w_id: 13 },
    "burning hell": { oc_id: 3, w_id: 13 },
    "compact feed mechanism": { oc_id: 4, w_id: 13 },
    "cfm": { oc_id: 4, w_id: 13 },
    "exhaust vectoring": { oc_id: 5, w_id: 13 },
    "ev": { oc_id: 5, w_id: 13 },
    "bullet hell": { oc_id: 6, w_id: 13 },
    "lead storm": { oc_id: 7, w_id: 13 },
    "ls ls": { oc_id: 7, w_id: 13 },
    "lsls": { oc_id: 7, w_id: 13 },
    /* AUTOCANNON */
    "composite drums": { oc_id: 1, w_id: 14 },
    "cd": { oc_id: 1, w_id: 14 },
    "splintering shells": { oc_id: 2, w_id: 14 },
    "splinter": { oc_id: 2, w_id: 14 },
    "carpet bomber": { oc_id: 3, w_id: 14 },
    "carpet bomb": { oc_id: 3, w_id: 14 },
    "carpet": { oc_id: 3, w_id: 14 },
    "cb": { oc_id: 3, w_id: 14 },
    "combat mobility": { oc_id: 4, w_id: 14 },
    "big bertha": { oc_id: 5, w_id: 14 },
    "bertha": { oc_id: 5, w_id: 14 },
    "bb": { oc_id: 5, w_id: 14 },
    "neurotoxin payload": { oc_id: 6, w_id: 14 },
    "neuro toxin payload": { oc_id: 6, w_id: 14 },
    "neuro": { oc_id: 6, w_id: 14 },
    "ntp": { oc_id: 6, w_id: 14 },
    /* BULLDOG */
    "chain hit bulldog": { oc_id: 1, w_id: 15 },
    "homebrew powder bulldog": { oc_id: 2, w_id: 15 },
    "volatile bullets": { oc_id: 3, w_id: 15 },
    "volatile bullet": { oc_id: 3, w_id: 15 },
    "vb": { oc_id: 3, w_id: 15 },
    "six shooter": { oc_id: 4, w_id: 15 },
    "elephant rounds": { oc_id: 5, w_id: 15 },
    "magic bullets": { oc_id: 6, w_id: 15 },
    "magic bullet": { oc_id: 6, w_id: 15 },
    "magic": { oc_id: 6, w_id: 15 },
    "mb": { oc_id: 6, w_id: 15 },
    /* BRT */
    "composite casings": { oc_id: 1, w_id: 16 },
    "cc": { oc_id: 1, w_id: 16 },
    "full chamber seal": { oc_id: 2, w_id: 16 },
    "full chamber": { oc_id: 2, w_id: 16 },
    "fcs": { oc_id: 2, w_id: 16 },
    "compact mags": { oc_id: 3, w_id: 16 },
    "experimental rounds": { oc_id: 4, w_id: 16 },
    "electro minelets": { oc_id: 5, w_id: 16 },
    "micro flechettes": { oc_id: 6, w_id: 16 },
    "flechettes": { oc_id: 6, w_id: 16 },
    "mf": { oc_id: 6, w_id: 16 },
    "lead spray": { oc_id: 7, w_id: 16 },
    "ls brt": { oc_id: 7, w_id: 16 },
    /* HURRICANE */
    "manual guidance cutoff": { oc_id: 1, w_id: 20 },
    "guidance cutoff": { oc_id: 1, w_id: 20 },
    "mgc": { oc_id: 1, w_id: 20 },
    "overtuned feed mechanism": { oc_id: 2, w_id: 20 },
    "fragmentation missiles": { oc_id: 3, w_id: 20 },
    "frag missiles": { oc_id: 3, w_id: 20 },
    "frag": { oc_id: 3, w_id: 20 },
    "plasma burster missiles": { oc_id: 4, w_id: 20 },
    "plasma bursters": { oc_id: 4, w_id: 20 },
    "plasma burster": { oc_id: 4, w_id: 20 },
    "pbm": { oc_id: 4, w_id: 20 },
    "minelayer system": { oc_id: 5, w_id: 20 },
    "mine layer system": { oc_id: 5, w_id: 20 },
    "minelayer": { oc_id: 5, w_id: 20 },
    "mine layer": { oc_id: 5, w_id: 20 },
    "jet fuel homebrew": { oc_id: 6, w_id: 20 },
    "jet fuel": { oc_id: 6, w_id: 20 },
    "jfh": { oc_id: 6, w_id: 20 },
    "salvo module": { oc_id: 7, w_id: 20 },
    "salvo": { oc_id: 7, w_id: 20 },
    "sm": { oc_id: 7, w_id: 20 },
    /* COILGUN */
    "ultra-magnetic coils": { oc_id: 1, w_id: 23, nyi: true },
    "ultra magnetic coils": { oc_id: 1, w_id: 23, nyi: true },
    "ultramagnetic coils": { oc_id: 1, w_id: 23, nyi: true },
    "umc": { oc_id: 1, w_id: 23, nyi: true },
    "backfeeding module": { oc_id: 2, w_id: 23, nyi: true },
    "back feeding module": { oc_id: 2, w_id: 23, nyi: true },
    "bfm": { oc_id: 2, w_id: 23, nyi: true },
    "re-atomizer": { oc_id: 3, w_id: 23, nyi: true },
    "reatomizer": { oc_id: 3, w_id: 23, nyi: true },
    "the mole": { oc_id: 4, w_id: 23, nyi: true },
    "mole": { oc_id: 4, w_id: 23, nyi: true },
    "hellfire": { oc_id: 5, w_id: 23, nyi: true },
    "hell fire": { oc_id: 5, w_id: 23, nyi: true },
    "triple-tech chambers": { oc_id: 6, w_id: 23, nyi: true },
    "triple tech chambers": { oc_id: 6, w_id: 23, nyi: true },
    "triple tech": { oc_id: 6, w_id: 23, nyi: true },
    "ttc": { oc_id: 6, w_id: 23, nyi: true },
    /* ----- */
    /* SCOUT */
    /* ----- */
    /* GK2 */
    "compact ammo": { oc_id: 1, w_id: 5 },
    "ca": { oc_id: 1, w_id: 5 },
    "gas rerouting": { oc_id: 2, w_id: 5 },
    "gr": { oc_id: 2, w_id: 5 },
    "homebrew powder gk2": { oc_id: 3, w_id: 5 },
    "overclocked firing mechanism": { oc_id: 4, w_id: 5 },
    "bullets of mercy": { oc_id: 5, w_id: 5 },
    "bom": { oc_id: 5, w_id: 5 },
    "ai stability engine": { oc_id: 6, w_id: 5 },
    "ai stability": { oc_id: 6, w_id: 5 },
    "ai stab": { oc_id: 6, w_id: 5 },
    "ase": { oc_id: 6, w_id: 5 },
    "electrifying reload": { oc_id: 7, w_id: 5 },
    "electric reload": { oc_id: 7, w_id: 5 },
    /* M1000 */
    "hoverclock": { oc_id: 1, w_id: 6 },
    "hover clock": { oc_id: 1, w_id: 6 },
    "hc": { oc_id: 1, w_id: 6 },
    "minimal clips": { oc_id: 2, w_id: 6 },
    "minimal clip": { oc_id: 2, w_id: 6 },
    "min clip": { oc_id: 2, w_id: 6 },
    "mc": { oc_id: 2, w_id: 6 },
    "active stability system": { oc_id: 3, w_id: 6 },
    "ass": { oc_id: 3, w_id: 6 },
    "hipster": { oc_id: 4, w_id: 6 },
    "electrocuting focus shots": { oc_id: 5, w_id: 6 },
    "efs": { oc_id: 5, w_id: 6 },
    "supercooling chamber": { oc_id: 6, w_id: 6 },
    "super cooling chamber": { oc_id: 6, w_id: 6 },
    "scc": { oc_id: 6, w_id: 6 },
    /* BOOMSTICK */
    "compact shells": { oc_id: 1, w_id: 7 },
    "cs": { oc_id: 1, w_id: 7 },
    "double barrel": { oc_id: 2, w_id: 7 },
    "db": { oc_id: 2, w_id: 7 },
    "special powder": { oc_id: 3, w_id: 7 },
    "sp": { oc_id: 3, w_id: 7 },
    "stuffed shells": { oc_id: 4, w_id: 7 },
    "shaped shells": { oc_id: 5, w_id: 7 },
    "jumbo shells": { oc_id: 6, w_id: 7 },
    "jumbo": { oc_id: 6, w_id: 7 },
    "js": { oc_id: 6, w_id: 7 },
    /* ZHUKOVS */
    "minimal magazines": { oc_id: 1, w_id: 8 },
    "custom casings": { oc_id: 2, w_id: 8 },
    "cryo minelets": { oc_id: 3, w_id: 8 },
    "cryo mines": { oc_id: 3, w_id: 8 },
    "embedded detonators": { oc_id: 4, w_id: 8 },
    "embedded dets": { oc_id: 4, w_id: 8 },
    "embed dets": { oc_id: 4, w_id: 8 },
    "gas recycling": { oc_id: 5, w_id: 8 },
    /* DRAK */
    "thermal liquid coolant": { oc_id: 1, w_id: 18 },
    "thermal liquid": { oc_id: 1, w_id: 18 },
    "tlc": { oc_id: 1, w_id: 18 },
    "aggressive venting": { oc_id: 2, w_id: 18 },
    "av": { oc_id: 2, w_id: 18 },
    "rewiring mod": { oc_id: 3, w_id: 18 },
    "rewiring": { oc_id: 3, w_id: 18 },
    "rm": { oc_id: 3, w_id: 18 },
    "impact deflection": { oc_id: 4, w_id: 18 },
    "bounce": { oc_id: 4, w_id: 18 },
    "bouncy": { oc_id: 4, w_id: 18 },
    "id": { oc_id: 4, w_id: 18 },
    "overtuned particle accelerator": { oc_id: 5, w_id: 18 },
    "opa": { oc_id: 5, w_id: 18 },
    "shield battery booster": { oc_id: 6, w_id: 18 },
    "sbb": { oc_id: 6, w_id: 18 },
    "thermal exhaust feedback": { oc_id: 7, w_id: 18 },
    "thermal exhaust": { oc_id: 7, w_id: 18 },
    "tef": { oc_id: 7, w_id: 18 },
    /* NISHANKA */
    "quick fire": { oc_id: 1, w_id: 24, nyi: true },
    "qf": { oc_id: 1, w_id: 24, nyi: true },
    "the specialist": { oc_id: 2, w_id: 24, nyi: true },
    "specialist": { oc_id: 2, w_id: 24, nyi: true },
    "cryo bolt": { oc_id: 3, w_id: 24, nyi: true },
    "cryo bolts": { oc_id: 3, w_id: 24, nyi: true },
    "fire bolt": { oc_id: 4, w_id: 24, nyi: true },
    "fire bolts": { oc_id: 4, w_id: 24, nyi: true },
    "bodkin points": { oc_id: 5, w_id: 24, nyi: true },
    "bodkin bolts": { oc_id: 5, w_id: 24, nyi: true },
    "bodkin": { oc_id: 5, w_id: 24, nyi: true },
    "trifork volley": { oc_id: 6, w_id: 24, nyi: true },
    "trifork": { oc_id: 6, w_id: 24, nyi: true },
    "tfv": { oc_id: 6, w_id: 24, nyi: true },
    "tv": { oc_id: 6, w_id: 24, nyi: true },
  }

  const Parser = {
    walkNodes: function (elem) {
      const drgNodeTreeWalker = document.createTreeWalker(
        elem,
        NodeFilter.SHOW_TEXT,
        {
          acceptNode: function (node) {
            if (!node.parentElement.classList.contains(TEXT_CLASS) && !node.parentElement.classList.contains(GREENTEXT_CLASS)) 
              return NodeFilter.FILTER_REJECT;
            if (node.parentElement.classList.contains(DRG_CLASS) || (node.parentElement.classList.contains(DRG_INVALID_CLASS)))
              return NodeFilter.FILTER_REJECT;
            if (node.nodeValue.match(DRG_REGEX))
              return NodeFilter.FILTER_ACCEPT;
          }
        },
        false,
      );
      while (drgNodeTreeWalker.nextNode()) {
        const node = drgNodeTreeWalker.currentNode;
        if (!node.parentElement.classList.contains(DRG_CLASS))
          Parser.linkify(node);
      }
    },

    modCodeToAlpha: function (modCode) {
      return modCode.replace(/1/g, "A").replace(/2/g, "B").replace(/3/g, "C").replace(/x/gi, "-");
    },

    wrapModCode: function (modCode, OC) {
      var e;

      // Mark Invalid Build
      if (OC === null) {
        e = document.createElement("span");
        e.classList = DRG_INVALID_CLASS;
        e.innerHTML = modCode;
      } else {
        // Mark Valid Build
        let alphaModCode = Parser.modCodeToAlpha(modCode);

        e = document.createElement("a");
        e.classList = DRG_CLASS;
        // Warn if Karl has yet to implement a stat page for the weapon
        if (OC.nyi === true) e.classList.add(DRG_NYI_CLASS);
        e.href = `https://karl.gg/asv/${OC.w_id}/${alphaModCode}${OC.oc_id}`
        e.innerHTML = "[Karl] ";
        if (OC.nyi === true) e.innerHTML += "[404] ";
        e.innerHTML += modCode;
        e.target = "_blank";
        e.rel = "noreferrer";
      }

      return e;
    },


    linkify: function (textNode) {
      const nodeOriginalText = textNode.nodeValue;
      const matches = [];

      let match;
      while (match = DRG_REGEX.exec(nodeOriginalText)) {
        matches.push({
          index: match.index,
          mod: match[1],
          name: match[2].toLowerCase()
        });
      }

      // Keep text in text node until first mod code
      textNode.nodeValue = nodeOriginalText.substring(0, matches[0].index);

      // Insert rest of text while linkifying mod codes
      let prevNode = null;

      for (let i = 0; i < matches.length; ++i) {

        let ocName = matches[i].name;
        const ocNameSpaces = (ocName.match(/ /g)||[]).length;
        let overclockObj;

        // Try to find a match for the OC name
        for (let j = 0; j <= ocNameSpaces; ++j) {
          if (ocName in OVERCLOCKS) {
            overclockObj = OVERCLOCKS[ocName];
            break;
          }
          ocName = ocName.substring(0, ocName.lastIndexOf(" "));
        }

        // Failed to find a matching OC
        if (overclockObj === undefined) {
          overclockObj = null;
        }

        // Insert linkified mod code
        let modLinkNode = Parser.wrapModCode(matches[i].mod, overclockObj);

        textNode.parentNode.insertBefore(
          modLinkNode,
          prevNode ? prevNode.nextSibling : textNode.nextSibling,
        );

        // Insert text after if there is any
        let upper;
        if (i === matches.length - 1)
          upper = undefined;
        else
          upper = matches[i + 1].index;
        let substring;
        if (substring = nodeOriginalText.substring(matches[i].index + 5, upper)) {
          const subtextNode = document.createTextNode(substring);
          textNode.parentNode.insertBefore(
            subtextNode,
            modLinkNode.nextElementSibling,
          );
          prevNode = subtextNode;
        }
        else {
          prevNode = modLinkNode;
        }
      }
    },
  }

  document.addEventListener("DOMContentLoaded", function () {

    // Only Run on Deep Rock Threads
    if ((CHECK_OP === true) && (window.location.pathname.split('/')[2] == "thread")) {
      let opPost = document.body.getElementsByClassName(OP_POST_CLASS)[0];
      let opSubject = opPost.getElementsByClassName(SUBJECT_CLASS)[0];
      let opMessage = opPost.getElementsByClassName(TEXT_CLASS)[0];
      if (!opSubject.innerText.match(OP_REGEX) && !opMessage.innerText.match(OP_REGEX)) return;
    }
    
    // Only attach CSS on 4channel
    if (HOSTNAME === "boards.4channel.org") {
      const style = document.createElement("style");
      style.innerHTML = css;
      document.head.appendChild(style);
    }

    Parser.walkNodes(document.body.getElementsByClassName(ROOT_CLASS)[0]);

    const observer = new MutationObserver(function (m) {
      for (let i = 0; i < m.length; ++i) {
        let addedNodes = m[i].addedNodes;

        for (let j = 0; j < addedNodes.length; ++j) {
          Parser.walkNodes(addedNodes[j]);
        }
      }
    });

    observer.observe(document.body.getElementsByClassName(ROOT_CLASS)[0], { childList: true, subtree: true })
  });
})();
