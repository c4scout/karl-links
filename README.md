# Karl Links
Karl Links is a userscript that linkifies posts with Deep Rock Galactic mod codes into [karl.gg](https://karl.gg) links.

Supports running on the archives as well.

The format should be `ModCode Overclock`:

`13212 Magic Bullets`

You may use `x` to leave a slot unpicked:

`1x2x2 Magic Bullets`

## Important Info
- As of U36, karl has [not yet implemented](https://github.com/drg-tools/karl.gg/issues/312) advanced stat pages for the new secondary weapons. These will show up with a `[404]` to denote that they are not yet functional.

- A few overclocks on different weapons share the same name. For these you will need to add the weapon name after the overclock name:

    `xxxxx Chain Hit Bulldog`

    `xxxxx Chain Hit Subata`

    `xxxxx Homebrew Powder Bulldog`

    `xxxxx Homebrew Powder GK2`

    `xxxxx Homebrew Powder Subata`

- Common nicknames and abbreviations are included, unless they have a naming conflict with another OC:

    `xxxxx BOM` works for `xxxxx Bullets of Mercy`. 

- Karl Links by default will try to only run on threads that it knows are DRG threads, to disable this behavior set:

    `const CHECK_OP = false;`

## Install
You will need a userscript manager, install one for your browser and then: 

**[Click Here to Install](https://gitlab.com/c4scout/karl-links/-/raw/main/karl-links.user.js)**

### Firefox
[Violentmonkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)

[Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)

[Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) 

### Chromium
[Violentmonkey](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag)

[Tampermonkey](https://tampermonkey.net/)
